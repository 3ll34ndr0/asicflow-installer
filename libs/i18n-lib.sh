#!/bin/bash
##
# Thin library around basic I18N facilitated function
#   basic text display, file logging, error display, and prompting
#
# as found in an example at:
# http://www.linuxjournal.com/content/internationalizing-those-bash-scripts
# original by Louis Iacona
#
# (Copyleft) D. Cuartielles, 2016, GPLv3


# echo -e "\033[30m#######################################################################\n"
function delimiter {
  echo -e "\033[30m#######################################################################\n"
}

###############################################
##
## Display some text to stderr
## $1 is assumed to be the Message Catalog key
function i18n_error {
        delimiter
        echo -e "\e[91m$(gettext -s "$1")\e[39m" >&2
}

###############################################
##
## Display some text to sdtout
## $1 is assumed to be the Message Catalog key
## rest of args are used as misc information
function i18n_display {
        typeset key="$1"
        shift
        delimiter
        echo -e "\e[92m$(gettext -s "$key") $@\e[39m"
}

###############################################
## Append a log message to a file.
## use $1 as target file to append to
## use $2 as catalog key
## rest of args are used as misc information
function i18n_fileout {
        [[ $# -lt 2 ]] && return 1
        typeset file="$1"
        typeset key="$2"
        shift 2
        echo "$(gettext -s "$key") $@" >> ${file}
}

## Prompt the user with a message and echo back the response.
## $1 is assumed to be the Message Catalog key
function i18n_prompt {
        typeset rv
        [[ $# -lt 1 ]] && return 1
        read -p $'\e[92m'"$(gettext "$1")"$': \e[39m' rv
        echo -e $rv
}
