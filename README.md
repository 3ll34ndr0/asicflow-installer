# asicflow-installer
Instalation scripts for an ASIC flow



## How to use it:
```
git clone https://gitlab.com/3ll34ndr0/asicflow-installer.git
cd asicflow-installer
bash install.sh
```


## Screenshots :
It is a command line, but we all love screenshots! Here is a little sample (don't worry, English and Portuguese version are also available):

![Welcome message (spanish)](/docs/welcome_es.png)

## Features

* Multi language (English, Spanish and Portuguese)
* It's a push and go tool
* Works on Debian and derivatives, but it's super easy to add new Distros. If you can't do it, just ask me.
* It's open for new contributors (just fork it and make a merge request)

## Available software:
* [Yosys](http://www.clifford.at/yosys/)
* [Qflow](http://opencircuitdesign.com/qflow/)
* [OpenSTA](https://github.com/abk-openroad/OpenSTA)
* [Icarus Verilog](http://iverilog.icarus.com/)
* [Magic](http://opencircuitdesign.com/magic/)



[![asciicast](https://asciinema.org/a/240109.svg)](https://asciinema.org/a/240109?speed=3)
