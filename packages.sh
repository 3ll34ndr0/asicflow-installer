#!/bin/bash
#
###############################################################################
# Tools Configuration Section. All packages and sources are hardcoded
# in this section.
###############################################################################
# The next packages are needed in order to build the other software:
MISC_DEPS="tcsh autoconf gperf build-essential clang bison flex libreadline-dev minisat gawk \
           tcl-dev libffi-dev git mercurial graphviz xdot pkg-config python cmake libx11-dev \
           tkcon tk-dev git libelf-dev libgsl-dev python3-tk swig"
INSTALL_MISC="$INSTALL_COMMAND $MISC_DEPS"
FPGA_DEPS="python3 libftdi-dev"

# These are the development versions (up to date may 2018)
MAGIC="git submodule update --init magic-8.2"
GRAYWOLF="git submodule update --init graywolf"
QROUTER="git submodule update --init qrouter-1.4"
QFLOW="git submodule update --init qflow"
DFLOWDOC="git submodule update --init dflow-doc"
NETGEN="git submodule update --init netgen-1.5"

# Common tools for the ASIC and FPGA Flow
IVERILOG="git submodule update --init iverilog"
YOSYS="git submodule update --init yosys"
OPENSTA="git submodule update --init OpenSTA"
#TODO:
GTKWAVE="svn .. "
# This sources are only for the FPGA Design Flow only:
ICESTORM="git clone https://github.com/cliffordwolf/icestorm.git"
ARACHNEPNR="git clone https://github.com/cseed/arachne-pnr.git"

##############################################################################################
##############################################################################################
##############################################################################################

