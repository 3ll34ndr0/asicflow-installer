#!/bin/bash
#
# This script is based on:
# D. Cuartielles installation script:
# https://github.com/dcuartielles/open-fpga-install.git

# I rewrote the main script and kept the translation functions found in libs and
# locale directories, with some minor modifications to color the output.

# Reminder: From time to time, remember to run:
# git submodule update --remote
# So it updates all submodules used for this installer.
#
cd locale
source generate_translations.sh
cd ..

# initialiaze the global variables needed for your default language
export TEXTDOMAINDIR=locale
export TEXTDOMAIN=install.sh
I18NLIB=libs/i18n-lib.sh

# source in I18N library - shown above
# this is the only message that cannot be translated
if [[ -f $I18NLIB ]]; then
   . $I18NLIB
else
   printf "ERROR - $I18NLIB NOT FOUND"
   exit 1
fi

###############################################################################
# Distribution Related Configuration
###############################################################################
# For Debian and derivatives
INSTALL_COMMAND="sudo apt install"
# Temporary dir for all sources
INSTALLTMP="sources" #TODO: I should also use a bin directory to throw binaries there
# This file imports all source packages urls and names.
source packages.sh

##############################################################################################
# Function Definitions Section:
#@brief It can install directly from the distribution repo or download sources, compile and install
#@param package name
#@param Download command to get the sources
downloadCompileAndinstall()
{
#  i18n_display "Download, Compile and Install tool:"
  i18n_display "$1" # TODO: Write a brief description in the .po file for every tool
  packageName=$1

  #discard first parameter and keep the remaining
  shift
  SOURCE_REPO=$*

  #Removes almost everything except the .git extension
#  SOURCEDIR=${SOURCE_REPO##*/}
#  echo $SOURCEDIR
#  Removes the .git "extension" if any
#  SOURCEDIR=${SOURCEDIR%.git}
  SOURCEDIR=${SOURCE_REPO##*--init }
#  echo $SOURCEDR

  # Ask user to choose:
  install=$(i18n_prompt "Choose repositories, sources or skip")
  case $install in
      i|I ) echo "$INSTALL_COMMAND $packageName"
                  $INSTALL_COMMAND $packageName;;
      c|C ) i18n_display "Download and compile $1"
            #cd $INSTALLTMP
            echo $SOURCE_REPO
                 $SOURCE_REPO
                 echo "cd $SOURCEDIR"
                 cd $SOURCEDIR

            ### This is needed for OpenSTA
            if [ -e bootstrap ]; then
                echo "./bootstrap"
                      ./bootstrap
            fi

            ### This is needed for iverilog
            if [ -e autoconf.sh ]; then
                echo "sh autoconf.sh"
                      sh autoconf.sh
            fi
            ### This is needed almost always
            if [ -x configure ]; then
                echo "./configure"
                      ./configure
            fi
            #
            if [ $packageName = "graywolf" ]; then
                echo "cmake ."
                      cmake .
            fi
            echo "make -j$CORES";
                  make -j$CORES
            echo "sudo make install";
                  sudo make install
            cd ../ ;;
      * ) echo;;#i18n_display "Skipped action";;
  esac
} # End of downloadCompileAndinstall Function

# Check if gettext is available in the system:
function isGettextAvailable ()
{
  command -v gettext >/dev/null 2>&1 || { echo >&2 "Please Install
                                        the gettext program before.
                                        Aborting.";
                                        exit 1;
                                      }
}


function isInstalled ()
{
  command -v $1 >/dev/null 2>&1 || { echo >&2 "Please Install
                                        the $1 program before.
                                        Aborting.";
                                        exit 1;
                                         }
}


#
function askSudoPrivileges ()
{
   sudo id &> /dev/null
   if [[ $? -ne 0 ]]; then
      i18n_error "Need sudo"
      exit 1
   fi
}

#@brief You give it a list of packages to be installed from distro repo.
#       But the first param is the FPGA or ASIC opcion
function install_dependencies()
{
  i18n_display "Installing dependencies"

  if      [ $1 = "ASIC" ]; then
      echo  " "
  else if [ $1 = "FPGA" ]; then
      echo  "fpgas"
      INSTALL_MISC=$FPGA_DEPS:$INSTALL_MISC
       fi
  fi

  install=$(i18n_prompt "Confirm installation")

  case $install in
      i|I )  i18n_display "Confirmed action"
             echo $INSTALL_MISC
                  $INSTALL_MISC;;
      s|S )  echo;;#i18n_display "Skipped action";;
  esac

} #End of install_dependencies


##################################################
# To install iverilog in ubuntu
#sudo add-apt-repository ppa:team-electronics/ppa
#sudo apt-get update
#sudo apt-get install iverilog

# This piece of code is wrapped in a function and was copied from D. Cuartieles install script.
deleteTempFiles ()
{
  # Delete temp files
  i18n_display "Delete temp files"
  install=$(i18n_prompt "Confirm deletion")
  if [[ $install == C* ]]; then
    i18n_display "Confirmed action"
    rm -fR $INSTALLTMP
  else
    i18n_display "Skipped action"
  fi
}



# @brief Avoid killing the pc while compiling (leaving a spare core).
useOneOrLeaveOne()
{
    CORES=`nproc`
    if [ $CORES -ge 1 ]; then
        CORES=$((`nproc` - 1))
#        i18n_display "Using number of cores: "
#        echo $CORES
    fi
}
# End of Function Definition Section


catfarolero() {
   if hash lolcat 2>/dev/null; then
      lolcat "$@"
   else
      cat "$@"
   fi
        }
############################################################################
###########################################################################
############################################################################


# "Main" Section:
# Logo
clear
banner=banner.txt

#
isGettextAvailable

# Display initial greeting
i18n_display "Greeting"
sleep 0.2
catfarolero $banner
i18n_display "Intro"
i18n_display "Press any key to continue..."
read -s -n 1 any_key
#
askSudoPrivileges

# If more than core, used all but one.
useOneOrLeaveOne

# Tools for the ASIC and FPGA flow:

#GTKWAVE
#YOSYS

##################################################
# Install the ASIC flow:

install_dependencies "ASIC" # build-essentials, gcc, etc
downloadCompileAndinstall iverilog $IVERILOG
downloadCompileAndinstall yosys $YOSYS
downloadCompileAndinstall graywolf $GRAYWOLF
downloadCompileAndinstall qrouter $QROUTER
downloadCompileAndinstall OpenSTA $OPENSTA
downloadCompileAndinstall magic $MAGIC
downloadCompileAndinstall netgen $NETGEN
# The order is important for qflow as
# it will check what STA engine is installed, etc.
downloadCompileAndinstall qflow $QFLOW
# This project is useful to test the flow
downloadCompileAndinstall dflow-doc $DFLOWDOC
#
##################################################


##################################################
# Install the FPGA Flow:
#install_dependencies "FPGA" # icestorm, archnepnr, etc
#downloadCompileAndinstall $ICESTORM
#downloadCompileAndinstall $ARACHNEPNR
##################################################

#printf "\n#######################################################################\n\n"
#i18n_display "Credits"
#printf "\n#######################################################################\n\n"
#i18n_display "Copyright"
#printf "\n#######################################################################\n\n"

# Install ends here
exit 0

